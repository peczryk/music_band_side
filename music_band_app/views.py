
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from rest_framework import generics

from .serializers import BandSerializer, BandInfoSerializer
from .models import Band, BandInfo, Comment

from django.shortcuts import render, get_object_or_404
from .filters import BandFilter
from .forms import BandForm, CommentForm
from django.utils import timezone

from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration\signup.html'

def index(request):
    newBands = Band.objects.filter(created_date__lte=timezone.now()).order_by('created_date')
    topBands = Band.objects.order_by('-totalOpinion')[:5]
    return render(request, 'index.html', {'newBands': newBands, 'topBands': topBands})

def ranking(request):

    ranking = Band.objects.order_by('-totalOpinion')
    return render(request, 'ranking.html', {'ranking': ranking})


def bandInformation(request, pk):
    band = get_object_or_404(Band, pk=pk)
    averages = []
    if band.musicOpinionNum != 0:
        averages.append(band.musicOpinion/band.musicOpinionNum)
    else:
        averages.append(0)
    if band.vocalOpinionNum != 0:
        averages.append(band.vocalOpinion / band.vocalOpinionNum)
    else:
        averages.append(0)
    if band.scenicCostumeOpinionNum != 0:
        averages.append(band.scenicCostumeOpinion / band.scenicCostumeOpinionNum)
    else:
        averages.append(0)
    if band.behaviorOnTheStageOpinionNum != 0:
        averages.append(band.behaviorOnTheStageOpinion / band.behaviorOnTheStageOpinionNum)
    else:
        averages.append(0)
    if band.contactWithAudienceOpinionNum != 0:
        averages.append(band.contactWithAudienceOpinion / band.contactWithAudienceOpinionNum)
    else:
        averages.append(0)
    if band.visualEfectsOpinionNum != 0:
        averages.append(band.visualEfectsOpinion / band.visualEfectsOpinionNum)
    else:
        averages.append(0)
    if band.contactWithBandOpinionNum != 0:
        averages.append(band.contactWithBandOpinion / band.contactWithBandOpinionNum)
    else:
        averages.append(0)


    return render(request, 'band_information.html', {'band': band, 'averages':averages})


def addCommentToBand(request, pk):
    band = get_object_or_404(Band, pk=pk)
    

    if request.method == "POST":
        if request.POST.get("music"):
            musicOpinion = float(request.POST.get("music"))
            band.musicOpinion += musicOpinion
            band.musicOpinionNum += 1
            band.score += musicOpinion
        if request.POST.get("vocal"):
            band.vocalOpinion += float(request.POST.get("vocal"))
            band.vocalOpinionNum += 1
            band.score += float(request.POST.get("vocal"))
        if request.POST.get("scenicCostume"):
            band.scenicCostumeOpinion += float(request.POST.get("scenicCostume"))
            band.scenicCostumeOpinionNum += 1
            band.score += float(request.POST.get("scenicCostume"))
        if request.POST.get("behaviorOnTheStage"):
            band.behaviorOnTheStageOpinion += float(request.POST.get("behaviorOnTheStage"))
            band.behaviorOnTheStageOpinionNum += 1
            band.score += float(request.POST.get("behaviorOnTheStage"))
        if request.POST.get("contactWithAudience"):
            band.contactWithAudienceOpinion += float(request.POST.get("contactWithAudience"))
            band.contactWithAudienceOpinionNum += 1
            band.score += float(request.POST.get("contactWithAudience"))
        if request.POST.get("visualEfects"):
            band.visualEfectsOpinion += float(request.POST.get("visualEfects"))
            band.visualEfectsOpinionNum += 1
            band.score += float(request.POST.get("visualEfects"))
        if request.POST.get("contactWithBand"):
            band.contactWithBandOpinion += float(request.POST.get("contactWithBand"))
            band.contactWithBandOpinionNum += 1
            band.score += float(request.POST.get("contactWithBand"))

        band.totalOpinion = (band.musicOpinion + band.vocalOpinion + band.scenicCostumeOpinion + band.behaviorOnTheStageOpinion +
        band.contactWithAudienceOpinion + band.visualEfectsOpinion + band.contactWithBandOpinion)/(band.contactWithBandOpinionNum*7)
        #(band.contactWithBandOpinionNum*7) - suma wszytkich opinii
        if request.POST.get("isRecommended") == "recommend":
            band.recommended += 1
            band.save()
        elif request.POST.get("isRecommended") == "notRecommend":
            band.notRecommended += 1
            band.save()

        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.bandName = band
            comment.author = request.user
            comment.save()
            return redirect('bandInformation', pk=band.pk)
    else:
        form = CommentForm()
    return render(request, 'add_comment_to_band.html', {'form': form, 'band': band})


def add_band(request):

    if request.method == "POST":

        form = BandForm(request.POST)
        if form.is_valid():
            Band = form.save(commit=False)
            Band.author = request.user
            Band.published_date = timezone.now()
            Band.save()
            return redirect('bandInformation', pk=Band.pk)
    else:

        form = BandForm()

    return render(request, 'add_band_form.html', {'form': form})


def search(request):
    band_list = Band.objects.all()
    band_filter = BandFilter(request.GET, queryset=band_list)

    return render(request, 'search.html', {'filter': band_filter})


class BandApi(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""
    queryset = Band.objects.all()
    serializer_class = BandSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new band."""
        serializer.save()


class BandApiDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http GET, PUT and DELETE requests."""

    queryset = Band.objects.all()

    serializer_class = BandSerializer


class BandInfoApi(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""
    queryset = BandInfo.objects.all()
    serializer_class = BandInfoSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new band."""
        serializer.save()


class BandInfoApiDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http GET, PUT and DELETE requests."""

    queryset = BandInfo.objects.all()
    serializer_class = BandInfoSerializer
