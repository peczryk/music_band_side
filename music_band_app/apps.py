from django.apps import AppConfig


class MusicBandAppConfig(AppConfig):
    name = 'music_band_app'
