from django.urls import path
from . import views
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import BandApi, BandInfoApi
from .views import BandApiDetailsView, BandInfoApiDetailsView
from django_filters.views import FilterView
from music_band_app.filters import BandFilter
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^BandApi/$', BandApi.as_view(), name="create"),
    url(r'^BandInfoApi/$', BandInfoApi.as_view(), name="create"),
    url(r'^BandApi/(?P<pk>[0-9]+)/$',
        BandApiDetailsView.as_view(), name="details"),
    url(r'^BandInfoApi/(?P<pk>[0-9]+)/$',
        BandInfoApiDetailsView.as_view(), name="details"),
    #url(r'^search/$', views.search, name='search'),
    url(r'^search/$', FilterView.as_view(filterset_class=BandFilter,
        template_name='search.html'), name='search'),
    #path('add_band', views.add_band(), name='add_band'),
    path('add_band', views.add_band, name='add_band'),
    path('ranking', views.ranking, name='ranking'),
    path('band/<int:pk>/', views.bandInformation, name='bandInformation'),
    url(r'^band/(?P<pk>\d+)/comment/$', views.addCommentToBand, name='addCommentToBand'),
    path('accounts/', include('django.contrib.auth.urls')), # new
    #path('accounts/', include('accounts.urls')), # new
    path('signup/', views.SignUp.as_view(), name='signup'),
]



urlpatterns = format_suffix_patterns(urlpatterns)