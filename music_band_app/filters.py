from .models import Band
import django_filters


class BandFilter(django_filters.FilterSet):
    bandName = django_filters.CharFilter(lookup_expr='icontains')
    musicType = django_filters.CharFilter(lookup_expr='icontains')
    bandMembers = django_filters.CharFilter(lookup_expr='icontains')
    voivodeship = django_filters.CharFilter(lookup_expr='icontains')
    city = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Band
        fields = ['bandName', 'musicType', 'bandMembers', 'country', 'voivodeship', 'city']


    def __init__(self, *args, **kwargs):
        super(BandFilter, self).__init__(*args, **kwargs)
        self.filters['bandName'].label = "Nazwa zespołu"
        self.filters['musicType'].label = "Gatunek muzyczny"
        self.filters['bandMembers'].label = "Liczba członków"
        self.filters['country'].label = "Kraj"
        self.filters['voivodeship'].label = "Województwo"
        self.filters['city'].label = "Miasto"

