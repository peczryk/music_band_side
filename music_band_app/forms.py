from django import forms

from .models import Band, BandInfo, Comment

class BandForm(forms.ModelForm):

    class Meta:
        model = Band
        fields = ('bandName', 'musicType', 'bandMembers', 'country', 'voivodeship', 'city', )
        labels = {'bandName':'Nazwa zespołu', 'musicType':'Gatunek muzyczny', 'bandMembers':'Liczba członków',
                  'country':'Kraj', 'voivodeship':'Rejon','city':'Miejscowość'}


class BandInfoForm(forms.ModelForm):

    class Meta:
        model = BandInfo
        fields = ('comment',)


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('text',)
        labels = {'author':'Autor','text':'Komentarz',}