from django.db import models
from django.utils import timezone


class Band(models.Model):
    bandName = models.CharField(max_length=50)
    musicType = models.CharField(max_length=20)
    bandMembers = models.IntegerField()
    country = models.CharField(max_length=20, default="")
    voivodeship = models.CharField(max_length=20)
    city = models.CharField(max_length=50)

    recommended = models.IntegerField(default=0)
    notRecommended = models.IntegerField(default=0)

    musicOpinion = models.FloatField(default=0)
    musicOpinionNum = models.IntegerField(default=0)
    vocalOpinion = models.FloatField(default=0)
    vocalOpinionNum = models.IntegerField(default=0)
    scenicCostumeOpinion = models.FloatField(default=0)
    scenicCostumeOpinionNum = models.IntegerField(default=0)
    behaviorOnTheStageOpinion = models.FloatField(default=0)
    behaviorOnTheStageOpinionNum = models.IntegerField(default=0)
    contactWithAudienceOpinion = models.FloatField(default=0)
    contactWithAudienceOpinionNum = models.IntegerField(default=0)
    contactWithBandOpinion = models.FloatField(default=0)
    contactWithBandOpinionNum = models.IntegerField(default=0)
    visualEfectsOpinion = models.FloatField(default=0)
    visualEfectsOpinionNum = models.IntegerField(default=0)

    score = models.IntegerField(default=0)
    totalOpinion = models.FloatField(default=0)

    created_date = models.DateTimeField(
        default=timezone.now)

    def __str__(self):
        return self.bandName


class BandInfo(models.Model):
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    comment = models.TextField()
    published_date = models.DateTimeField(
        blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()


class Comment(models.Model):
    bandName = models.ForeignKey('music_band_app.Band', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def approved_comments(self):
        return self.comments.filter(approved_comment=True)
    def __str__(self):
        return self.text