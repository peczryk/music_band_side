from rest_framework import serializers
from .models import Band, BandInfo



class BandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Band
        fields = ('id','bandName', 'musicType', 'bandMembers', 'voivodeship', 'city', 'recommended', 'notRecommended', 'created_date')
        read_only_fields = ('recommended', 'notRecommended', 'created_date')


class BandInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = BandInfo
        fields = ('id', 'band', 'comment', 'published_date')
        read_only_fields = ('published_date',)



