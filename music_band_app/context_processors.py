from .models import Band, Comment
from django.shortcuts import get_object_or_404

def add_variable_to_context(request):
    lastAdded = Band.objects.order_by('-created_date')[:5]
    lastCommented = Comment.objects.order_by('-created_date')[:5]

    bands = []
    for comment in lastCommented:
        bands.append(get_object_or_404(Band, bandName=comment.bandName))
    return {
         'lastAdded': lastAdded, 'lastCommented': bands
    }
